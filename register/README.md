# Como rodar

Para esta aplicação foi utilizado a API django com python, o qual será resposável de fornecer as ferramentas necessárias para construir o backend.

* Comando para instalação do django:

    `python -m pip install Django`

* Para executar o servidor, rode o comando:

    `python manage.py runserver `


O servidor será levantado no endereço **http://127.0.0.1:8000/**. 

A aplicação roda a partir do endereço **http://127.0.0.1:8000/register**. Está já uma limitação, redirecionamento para pagina do aplicativo pode ser implementada futuramente caso necessário.

## Página Inicial

![Initial View](views/TelaInicial.PNG)


Na página principal existem duas opções:
    - Registro de novo usuário
    - Listagem dos usuários registrados
    
## Página de registro

![User Register](views/RegistroUsuario.PNG)

Nos campos apresentados, são inserido os dados de nome, cpf (com limite de 11 caracteres) e as opções de etapa atual.

No campo de etapa é possível, apenas, selecionar as opções disponíveis:

![User Register](views/RegistroOpcoes.png)


## Página de listagem dos usuários cadastrados

![Users List](views/UsersList.PNG)

A principal limitação do que fora desenvolvido nesta página, é que não é possível alterar a etapa atual do usuário.

# O que não foi realizado

Na página onde é dado a lista de todos os usuários cadastrados no banco não é utilizado o método GET e nem utilizado a estrutura json, porém, são utilizados atributos que a própria API do django oferece e a integração da própria sintaxe para html. 

Outro ponto, já comentado, seria de que não é possível editar a etapa do usuário pela lista, sendo apenas possível pelo menu do administrador.

Essas limitações se dão ao fato de que, primeiramente tive de ter uma curva de aprendizado para entender como funciona o Django. E como não tenho experiencia com webdesign foi necessário estudos sobre formatação e funcionamentos para poder implementar o frontend minimamente funcional para este teste.

Obs.: Esse tipo de aplicação é uma novidade para mim realizar a implementação, porém, foi divertido aprender todas essas coisas novas.