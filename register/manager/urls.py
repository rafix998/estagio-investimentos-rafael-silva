from django.urls import path

from . import views

app_name = "manager"

urlpatterns = [
    path(r'', views.InitialView, name="initial-view"),
    path(r'registration-list/', views.RegistrationList, name="registration-list"),
    path(r'user-registration/', views.UserRegistration, name="user-registration"),
    path(r'user-registration/confirmation-of-registration', views.FinishRegister, name="confirmation-of-registration")
]