from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.views.generic import ListView
from django.template import loader
#from rest_framework.decorators import api_view
from django.http.response import JsonResponse
#from rest_framework.parsers import JSONParser

from .models import Post
from .forms import PostForm

def InitialView(request):
    return render(request, 'manager/initial-view.html')

def UserRegistration(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()      
            return redirect('manager:confirmation-of-registration')
    else:
        form = PostForm()
    return render(request, 'manager/user_registration.html', {'form': form})

#@api_view(['GET', 'POST', 'PUT'])
def RegistrationList(request):
    try:
        latest_registration_list = Post.objects.order_by('-updated')
        template = loader.get_template('manager/registration_list.html')
        context = {
            'latest_registration_list' : latest_registration_list,
        }        
        return render(request, 'manager/registration_list.html', context)
    except:
        return Http404()

def FinishRegister(request):
    return render(request, 'manager/confirmation-of-registration.html')