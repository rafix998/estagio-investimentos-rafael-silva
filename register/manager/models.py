from django.db import models



class Post(models.Model):
    STAGE_CHOICE = (
        ('Aguardando assinatura de documentos','Aguardando assinatura de documentos'),
        ('Aguardando transferência de recursos','Aguardando transferência de recursos'),
        ('Gestão de patrimônio ativa','Gestão de patrimônio ativa')
    )

    name = models.CharField(max_length=255)
    cpf = models.CharField(max_length=11)
    stage = models.CharField(max_length=50, choices=STAGE_CHOICE)

    # Keep storage what time was created and the last time it was updated
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
